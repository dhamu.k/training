def insertion_sort(List):
    for i in range(1, len(List)):
        j = i-1
        nxt_element = List[i]		
        while (List[j] > nxt_element) and (j >= 0):
            List[j+1] = List[j]
            j=j-1
            List[j+1] = nxt_element

list =list(map(int,input().split()))
insertion_sort(list)
print(list)
