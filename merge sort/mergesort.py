def merge_sort(arr):
        if len(arr)<=1:
            return arr
        #middle
        middle=len(arr)//2
        left_list=arr[:middle]
        right_list=arr[middle:]
        left_list=merge_sort(left_list)
        right_list=merge_sort(right_list)
        return list(merge(left_list,right_list))
def merge(left_list,right_list):
    res=[]
    while len(left_list)!=0 and len(right_list)!=0:
        if left_list[0]<right_list[0]:
            res.append(left_list[0])
            left_list.remove(left_list[0])
        else:
            res.append(right_list[0])
            right_list.remove(right_list[0])
    if len(left_list)==0:
            res=res+right_list
    else:
            res=res+left_list
    return res
arr=list(map(int,input().split()))
print(merge_sort(arr))
