from flask import Flask,render_template,request,redirect,url_for
app = Flask(__name__)
List=["dhamu","sam","kd"]
@app.route('/get')
def home():
    return render_template("get.html",List=List)
@app.route('/put',methods=["POST","GET"])
def put():
    if request.method=="POST":
        mn=request.form["m"]
        List.append(mn)
        return redirect(url_for("home",List=List))
    else:
        return render_template("add.html")
@app.route('/delete',methods=["POST","GET"])
def deletet():
    if request.method=="POST":
        mn=request.form["m"]
        List.remove(mn)
        return redirect(url_for("home",List=List))
    else:
        return render_template("delete.html")

if __name__ == '__main__':
    app.run(debug=True,port=8041)