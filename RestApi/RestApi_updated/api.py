'''
Created on 3 Sep, 2020

@author: KD
'''
from flask import Flask
from flask.json import jsonify
app = Flask(__name__)
Student={1:"abishek",2:'anandh',3:"dhamu",4:"eshwar"}
@app.route('/students/get')
def home():
    return jsonify(Student)
@app.route('/student/get/<id1>')
def get(id1):
    
    return jsonify(Student.get(id1))
@app.route('/students/put/<int:id1>/<name>')
def put(id1,name):
    Student[id1]=name
    return jsonify(Student)
@app.route('/students/post/<int:id1>/<name>')
def post(id1,name):
    Student[id1]=name
    return jsonify(Student)
@app.route('/students/delete/<int:id1>')
def deletet(id1):
    Student.pop(id1)
    return jsonify(Student)

if __name__ == '__main__':
    app.run(debug=True,port=8041)
